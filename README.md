# PositiviTea Website
This is the frontend for the PositiviTea website.

## Requirements
* [Node.js](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/en/)
* [React](https://reactjs.org/)
* [TypeScript](https://www.typescriptlang.org/)
* [MobX](https://mobx.js.org/)
